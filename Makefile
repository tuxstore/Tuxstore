build:
	RUSTFLAGS="-C target-cpu=native opt-level=2"
	cargo build --release

clear:
	rm -r target/ &
	rm *.tar.gz &
	rm *pkg.tar.zst &
	rm src/LICENSE &
	rm src/tuxstore &
	rm src/*.tar.gz

clean:
	make clear

install:
	cp target/release/tuxstore-rust /usr/bin/tuxstore

uninstall:
	rm /usr/bin/tuxstore

test:
	RUSTFLAGS="-C target-cpu=x86_64 opt-level=2"
	cargo run

arch:
	cargo install cargo-aur
	cargo aur
	makepkg

ebuild:
	cargo install cargo-ebuild
	cargo ebuild
